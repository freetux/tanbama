## Tanbama

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

An example Django REST framework project for test driven development.

Read this in other languages: [فارسی](https://gitlab.com/biomanw/tanbama/blob/master/READMEFA.md)

### API Endpoints


#### user


* **/api/user/** (User registration endpoint)

allow methods:post
example:
POST:   
```
curl -X POST  -d "email=youremail&password=yourpassword" tanbama.ir/api/user/
```
output:

  give you a token and your user data.


* **/api/user/api-token-auth/**

provides JSON Web Token Authentication
allow methods:post
example:
POST:
```
curl -X POST -d "username=yourusername&password=yourpassword" tanbama.ir/api/api-token-auth/
```
output:
  token: "yourtoken" and "user data"

* **/api/user/api-token-refresh/**

  your tokens can be "refreshed" to obtain a brand new token with renewed expiration time.

allow methods:post
example:
POST:
```
curl -X POST -H "Content-Type: application/json" -d '{"token":"<EXISTING_TOKEN>"}' tanbama.ir/api/api-token-refresh/
```


#### body/weights

* **/api/body/weights/** (weights and height create and list endpoint)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/weights
```

output:

  list of your height and weights

POST
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"weight":70,"height":182,"text":"demo"}' -X POST tanbama.ir/api/body/weights
```

output:

  create an weight and height to database

* **/api/body/weights/{weight-id}/** (weight and height retrieve, update and destroy endpoint)


#### body/drug


* **/api/body/drug/** (drug create and list endpoint)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/drug
```

output:

  list of drugs

POST:

```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"name":drug-name,"description":"drug-description"}' -X POST tanbama.ir/api/body/drug
```

output:

  add a drug to database but just stuff users can add drug!

* **/api/body/drug/{drug-id}/** (drug retrieve, update and destroy endpoint)

  and also just stuff users can update and destroy drug!



#### body/rx


* **/api/body/rx/** (rx create and list endpoint)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/rx
```

output:

  list of rxs for each user
POST:

```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"user":user,"drugs":"drugslist"}' -X POST tanbam  a.ir/api/body/rx
```

output:

  add a rx to database but just stuff users can add rx!

* **/api/body/rx/{rx-id}/** (rx retrieve, update and destroy endpoint)

  and also just stuff users can update and destroy rx!

### Install

```
pip install -r requirements.txt
```
### Usage

#### how to development:
 
 first you need Clone the repository


```
git clone https://gitlab.com/biomanw/tanbama.git
```


#### Checkout the branch dev

```
git checkout dev
```

#### make changes and then :

```
git add .
#Adds the file to your local repository and stages it for commit.
```

```
git commit -m "your comment for this commit"
#Commits the tracked changes and prepares them to be pushed to a remote repository.
```

```
git push origin dev
#Pushes the changes in your local repository up to the remote repository you specified as the origin
```

dependencies:


1.python 3

2.pip package management system

### Install

1.make a python3 virtualenv
```
virtualenv -p python3 tanbamaenv
```

2.go to virtualenv


```
source tanbamaenv/bin/activate
```


3.install requirements


```
pip install -r requirements.txt
```

now everything is ok and you can cd to ranbamaapp directory and run server


```
cd tanbamaapp
python manage.py runserver
```



TODO:


- [x] restfull api
- [ ] clients
    - [ ] web
    - [ ] android
    - [ ] ios
