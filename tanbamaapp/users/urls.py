from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import  (login, 
                                    logout,     
                                    password_reset, 
                                    password_reset_done,
                                    password_reset_confirm, 
                                )
from .views import (activate, activation_complete, register,
                    registration_closed, registration_complete)

urlpatterns = [
    url(r'^register/$', register, name='users_register'),
    url(r'^register/closed/$', registration_closed,
        name='users_registration_closed'),
    url(r'^register/complete/$', registration_complete,
        name='users_registration_complete'),
    url(r'^activate/complete/$', activation_complete,
        name='users_activation_complete'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='users_activate'),

    url(r'^login/$', auth_views.login, {'template_name': 'users/pages-login.html'}),

    url(r'^logout/$', auth_views.logout, {'template_name': 'users/logout.html'}),


    url(r'^password_change/$', auth_views.password_change, {'template_name': 'users/password_change_form.html'
    , 'post_change_redirect' : 'users_password_change_done', 'name' : 'users_password_change'}),

    url(r'^password_change/done/$', auth_views.password_change_done, {'template_name': 'users/password_change_done.html'
    , 'name':'users_password_change_done'}),

    url(r'^password_reset/$', auth_views.password_reset, {'template_name': 'users/password_reset_form.html'}),
    url(r'^password_reset/done/$',
        auth_views.password_reset_done, {'template_name': 'users/password_reset_done.html'}),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, {'template_name': 'users/password_reset_confirm.html',
         'name':'users_password_reset_confirm', 'post_change_redirect':'users_password_reset_complete'}),

    url(r'^reset/done/$', auth_views.password_reset_complete, {'template_name': 'users/password_reset_complete.html',
         'name':'users_password_reset_complete'}),
]
