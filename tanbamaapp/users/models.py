from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.contenttypes.models import ContentType
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from .conf import settings
from .managers import UserInheritanceManager, UserManager
from django_jalali.db import models as jmodels

class AbstractUser(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        ('M', _("مرد")),
        ('F', _("زن"))
    )
    
    USERS_AUTO_ACTIVATE = not settings.USERS_VERIFY_EMAIL

    first_name = models.CharField(_('نام'), max_length=255 , null=True)
    last_name = models.CharField(_('نام خانوادگی'), max_length=255 , null=True)
    birthday = jmodels.jDateTimeField(_('تاریخ تولد'), null=True)
    gender = models.CharField(_('جنسیت'),max_length=1, null=True, choices=GENDER_CHOICES, default='M')

    email = models.EmailField(
        _('آدرس ایمیل'), max_length=255, unique=True, db_index=True)
    is_staff = models.BooleanField(
        _('وضعیت کاربر'), default=False,
        help_text=_('تعیین اینکه آیا کاربر میتواند به بخش مدیریت وارد شود یا خیر.'))

    is_active = models.BooleanField(
        _('فعال'), default=USERS_AUTO_ACTIVATE,
        help_text=_('مشخص میکند که کاربر فعال است یا خیر '
                    'به جای حذف حساب کاربری این قسمت را غیرفعال کنید.'))
    date_joined = jmodels.jDateTimeField(_('زمان عضویت'), auto_now_add=True, blank=True)
    user_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True, editable=False)

    objects = UserInheritanceManager()
    base_objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        abstract = True
    

    def get_full_name(self):
        
        return "{} {}".format( self.first_name, self.last_name )
    
    def get_short_name(self):
        """ Return the first_name."""
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """ Send an email to this User."""
        send_mail(subject, message, from_email, [self.email])

    def activate(self):
        self.is_active = True
        self.save()

    def save(self, *args, **kwargs):
        if not self.user_type_id:
            self.user_type = ContentType.objects.get_for_model(self, for_concrete_model=False)
        super(AbstractUser, self).save(*args, **kwargs)


class User(AbstractUser):

    """
    Concrete class of AbstractUser.
    Use this if you don't need to extend User.
    """
    is_doctor = models.BooleanField('docktor status', default=False)
    is_patient = models.BooleanField('patient status', default=True)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


class Patient(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    ssn = models.CharField(max_length=9, unique=True)
    street = models.CharField(max_length=50)
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=20)
    zip_code = models.CharField(max_length=5)

    def __unicode__(self):
        return self.user.first_name + ' ' + self.user.last_name


class DoctorSpeciality(models.Model):
    specialty = models.CharField(max_length=30, primary_key=True)

    def __unicode__(self):
        return self.specialty

    class Meta:
        verbose_name_plural = 'Doctors specialties'

class Doctor(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    specialty = models.ForeignKey(DoctorSpeciality)


    def __unicode__(self):
        return self.user.first_name + ' ' + self.user.last_name