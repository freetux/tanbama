# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-10 09:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('body', '0007_gbw'),
    ]

    operations = [
        migrations.CreateModel(
            name='Food',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='نام')),
                ('amount', models.FloatField(verbose_name='مقدار')),
                ('unit', models.CharField(choices=[('GR', 'گرم'), ('LT', 'لیتر'), ('NM', ' عدد')], max_length=2, verbose_name='واحد اندازه گیری')),
                ('Calories', models.FloatField(verbose_name='کالری')),
                ('details', models.CharField(max_length=255, null=True, verbose_name='توضیح')),
            ],
        ),
    ]
