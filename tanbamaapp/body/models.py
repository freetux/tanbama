from __future__ import unicode_literals
from django.conf import settings
#from rest_framework.authtoken.models import Token
from django.db import models
from django.utils.encoding import smart_text as smart_unicode
from django.utils.translation import ugettext_lazy as _
from django_jalali.db import models as jmodels

class Weight(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.CharField(_('توضیح'), max_length=255,  null=True)
    weight = models.FloatField(_('وزن'))
    height = models.FloatField(_('قد'))
    date_created = jmodels.jDateTimeField(_("تاریخ ایجاد"), auto_now_add=True)

    class Meta:
        verbose_name = _("Weight")
        verbose_name_plural = _("Weights")

    def __unicode__(self):
        return smart_unicode(self.text)
class Drug(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(_("نام دارو"), max_length=255)
    salt = models.CharField(_("ملح"), max_length=255)
    dosage_form = models.CharField(_("شکل دارویی"), max_length=255)
    strengh = models.CharField(_("مقدار دارو"), max_length=255)
    route_of_admin = models.CharField(_("راه مصرف"), max_length=255)
    atcـcode = models.CharField(_("دسته دارو"), max_length=255)
    ingredient = models.CharField(_("ترکیبات"), max_length=255)
    remarks = models.CharField(_("توضیحات"), max_length=255)
    date = jmodels.jDateTimeField(_("تاریخ ایجاد"),  null=True)

# this is for food program
class FoodProgram(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    breakfest = models.CharField(_("صبحانه"), max_length=255)
    bm = models.CharField(_("میان وعده صبحانه"), max_length=255)
    lounch = models.CharField(_(" نهار"), max_length=255)
    lm = models.CharField(_("میان وعده نهار"), max_length=255)
    dinner = models.CharField(_(" شام"), max_length=255)
    dm = models.CharField(_(" میان وعده شام"), max_length=255)
    date = jmodels.jDateTimeField(_("تاریخ ایجاد"),  null=True)


#goal body weight
class Gbw(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    gbw = models.FloatField(_(' وزن هدف'))
    date_created = jmodels.jDateTimeField(_("تاریخ ایجاد"), auto_now_add=True)




# add user activity level table to database
class UserActivity(models.Model):
    ACTIVITY_CHOICES = (
        ('VL', 'بدون فعالیت'),
        ('L', 'کم فعالیت'),
        ('N', ' فعالیت متوسط'),
        ('A', ' فعال'),
        ('VA', ' بیش فعال'),

    )
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    activity = models.CharField(_('میزان فعالیت روزانه'), max_length=2, choices=ACTIVITY_CHOICES)


class Rx(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    drugs = models.ManyToManyField(Drug, related_name="rxs")
    date_created = jmodels.jDateTimeField(_("Date Created"), auto_now_add=True)
    class Meta:
        verbose_name = _("Rx")
        verbose_name_plural = _("Rxs")

    def __unicode__(self):
        return "{}-{}-{}-{}".format(self.id, self.user, self.date_created, self.drugs)


class Food(models.Model):
    UNIT_CHOICES = (
        ('GR', 'گرم'),
        ('LT', 'لیتر'),
        ('NM', ' عدد')
    )
    id = models.AutoField(primary_key=True)
    name = models.CharField(_('نام'), max_length=255,  null=True)
    amount = models.FloatField(_('مقدار'))
    unit = models.CharField(_('واحد اندازه گیری'), max_length=2, choices=UNIT_CHOICES)
    calories = models.FloatField(_('کالری'))
    details = models.CharField(_('توضیح'), max_length=255,  null=True)




# def bmi(self):
#     return self.weight / (self.height/100) ** 2

#TODO:add BMI calculator
#TODO:add BMR calculator
#TODO:add BodyFat calculator
#TODO:add WHR calculator
#TODO:add IBW calculator
#TODO:add HRtR calculator