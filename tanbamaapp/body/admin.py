from django.contrib import admin
from body.models import Weight


class WeightAdmin(admin.ModelAdmin):
    list_display = ("user", "text", "weight", "date_created")
    list_filter = ("weight","date_created")


admin.site.register(Weight, WeightAdmin)
