from django.conf.urls import url
from sites import views
from body.models import Weight, Drug, Food




urlpatterns = [
    # Matches any html file - to be used for tanbama
    # Avoid using your .html in your resources.
    # Or create a separate django app.
    # url(r'^.*\.html', views.SitehtmlView.as_view(), name='tanbama'),

    # The home page
    url(r'^$', views.index, name='index'),
    url(r'^weight/list$', views.WeightList.as_view(), name='weights_list'),
    # url(r'^food/list$', views.FoodList.as_view(), name='food_list'),
    url(r'^users/list$', views.UserList.as_view(), name='users_list'),
    url(r'^home/$', views.HomeView.as_view(), name='dashboard'),
    url(r'^profile/(?P<pk>[\w]+)/$', views.ProfileView.as_view(), name='profile'),
    url(r'^profile/edit/(?P<pk>[\w]+)/$', views.ProfileUpdate.as_view(), name='profile_edit'),
    url(r'^rx/$', views.RxView.as_view(), name='rx'),
    url(r'^goal/$', views.GoalView.as_view(), name='goal'),
    url(r'^goal/create$', views.GoalCreate.as_view(), name='gbw_create'),
    url(r'^program/create$', views.FoodProgramCreate.as_view(), name='program_create'),
    url(r'^food/$', views.FoodList.as_view(), name='food_list'),
    url(r'^food/create$', views.FoodCreate.as_view(), name='food_create'),
    url(r'^food/(?P<pk>[\w]+)/$', views.FoodShow.as_view(), name='food_show'),
    url(r'^food/update/(?P<pk>[\w]+)/$', views.FoodUpdate.as_view(
        model=Food,
    ), name='food_update'),
    url(r'^food/delete/(?P<pk>[\w]+)/$', views.FoodDelete.as_view(
        model=Food,
    ), name='food_delete'),
    url(r'^drug/$', views.DrugList.as_view(), name='drug'),
    url(r'^drug/create$', views.CreateDrug.as_view(), name='drug_create'),
    url(r'^weight/create$', views.WeightCreate.as_view(), name='weight_create'),
    url(r'^weight/(?P<pk>[\w]+)/$', views.WeightShow.as_view(), name='weight_show'),
    url(r'^weight/update/(?P<pk>[\w]+)/$', views.WeightUpdate.as_view(
        model=Weight,
    ), name='weight_update'),
    url(r'^weight/delete/(?P<pk>[\w]+)/$', views.WeightDelete.as_view(
        model=Weight,
    ), name='weight_delete'),
]