from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from users.models import User
from django.conf import settings
from body.models import Weight, Gbw, Drug, Food, FoodProgram
from django_select2.forms import Select2MultipleWidget

class UserCreationForm(forms.ModelForm):
    email = forms.EmailField(label='ایمیل', max_length=255)
    password1 = forms.CharField(label='گذرواژه', widget=forms.PasswordInput)
    password2 = forms.CharField(label='گذرواژه مجدد', widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ('email',)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("email is taken")
        return email

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2


class WeightCreateForm(forms.ModelForm):
    """                                                                                                                            
    Use to create Weight                                                                                                           
    """
    class Meta(object):
        model = Weight
        fields = ('weight', 'height', 'text')
        # user will be set in views.ResumeNew                                                                                      
        # other fields will be set as model default                                                                                

class DrugCreateForm(forms.ModelForm):
    """                                                                                                                            
    Use to create Weight                                                                                                           
    """
    salt = forms.CharField(label="ملح" , max_length=255, required=False)
    dosage_form = forms.CharField(label= "شکل دارویی", max_length=255, required=False)
    strengh = forms.CharField(label="مقدار دارو", max_length=255, required=False)
    route_of_admin = forms.CharField(label="راه مصرف", max_length=255, required=False)
    atcـcode = forms.CharField(label="دسته دارو", max_length=255, required=False)
    ingredient = forms.CharField(label="ترکیبات", max_length=255, required=False)
    remarks = forms.CharField(label="توضیحات", max_length=255, required=False)
    class Meta(object):
        model = Drug
        fields = ('name', 'salt', 'dosage_form', 'strengh', 'route_of_admin', 'atcـcode', 'ingredient', 'remarks')


class GbwCreateForm(forms.ModelForm):
    """                                                                                                                            
    Use to create Weight                                                                                                           
    """
    class Meta(object):
        model = Gbw
        fields = ('gbw',)

class FoodCreateForm(forms.ModelForm):
    """                                                                                                                            
    Use to create Food                                                                                                           
    """
   
    class Meta(object):
        model = Food
        fields = ('name','amount', 'unit', 'calories', 'details')

class FoodProgramForm(forms.ModelForm):
    """
    this is for food program
    """
    breakfest = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    bm = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    lounch = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    lm = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    dinner = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    dm = forms.ModelMultipleChoiceField(queryset=Food.objects.values_list('name', flat=True).all(), widget=Select2MultipleWidget)
    class Meta(object):
        model = FoodProgram
        fields = ('breakfest','bm', 'lounch', 'lm', 'dinner','dm')


class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'is_active', 'is_staff')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]



class ProfileUpdateForm(forms.ModelForm):
    birthday = forms.DateField(label='تاریخ تولد',input_formats=settings.DATE_INPUT_FORMATS)
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'birthday', 'gender','email')