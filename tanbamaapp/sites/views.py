from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, CreateView, DetailView, UpdateView, DeleteView, ListView
from .forms import UserCreationForm, WeightCreateForm, ProfileUpdateForm, GbwCreateForm, DrugCreateForm, FoodCreateForm, FoodProgramForm
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from body.models import Weight, Rx, Drug, Gbw, Food, FoodProgram
from django.http import JsonResponse
from django.template.loader import render_to_string
from users.models import User
from jdatetime import date 
from django.utils.decorators import method_decorator
from users.decorators import patient_required, patient_required
from django.core.urlresolvers import reverse_lazy
from django.contrib.admin.views.decorators import staff_member_required
from pinax.messages.models import Thread
from pinax.messages.forms import Message
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# from usda_nutrition.models import FoodGroup, FoodDescription, NutrientDefinition, Weight


def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/home/")
    else:
        context = {}
        template = loader.get_template('sites/index.html')
        return HttpResponse(template.render(context, request))
        



    def dispatch(self, *args, **kwargs):
        return super(InboxView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InboxView, self).get_context_data(**kwargs)
        if self.kwargs.get("deleted", None):
            threads = Thread.ordered(Thread.deleted(self.request.user))
            folder = "deleted"
        else:
            threads = Thread.ordered(Thread.inbox(self.request.user))
            folder = "inbox"

        context.update({
            "folder": folder,
            "threads": threads,
            "threads_unread": Thread.ordered(Thread.unread(self.request.user))
        })
        return context


@method_decorator(login_required, name='dispatch')
class HomeView(TemplateView):
    template_name = 'sites/dashboard.html'
    
    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


    def get_context_data(self, **kwargs):
        dataset = {}
        dataset = []
        threads = Thread.ordered(Thread.inbox(self.request.user))
        folder = "inbox"
        
    # Iterate through the data in `Revenue` model and insert in to the `dataSource['data']` list.
        for key in Weight.objects.filter(user=self.request.user):
            data = {}
            data['weight'] = key.weight
            data['date_created'] = key.date_created
            dataset.append(data)
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context.update({
            "folder": folder,
            "threads": threads,
            "threads_unread": Thread.ordered(Thread.unread(self.request.user))
        })
        context['dataset'] = dataset
        return context

@method_decorator(login_required, name='dispatch')
class ProfileView(DetailView):
    model = User
    template_name = 'sites/profile.html'
        



@method_decorator(login_required, name='dispatch')
class RxView(TemplateView):
    template_name = 'sites/rxs.html'
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        rxs = Rx.objects.filter(user = self.request.user)
        context['rxs'] = rxs
        return context



@method_decorator([login_required, patient_required], name='dispatch')
class GoalView(TemplateView):
    template_name = 'sites/goal.html'
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        weight = Weight.objects.filter(user= self.request.user).values('weight',)
        person = User.objects.get(id= self.request.user.id)
        gender = person.gender
        born = person.birthday
        last_weight = weight.order_by('-id')[0]
        age = WeightList.age(self, born)
        
        
        lastest_weight = last_weight["weight"]
        height = Weight.objects.filter(user= self.request.user).values('height',)
        lastest_height = height.order_by('-id')[0]["height"]
        bmr = WeightList.bmr(self, lastest_weight, lastest_height, age, gender)
        wibmr_one = bmr + 1000
        wibmr_half = bmr +500
        wlbmr_one = bmr - 1000
        wlbmr_half = bmr -500

        lhi = WeightList.toinch(self, lastest_height)
        ibw = WeightList.ibw(self, gender, lhi)
        goalw = Gbw.objects.filter(user= self.request.user).values('gbw',)
        if goalw:
            gbw = goalw.order_by('-id')[0]["gbw"]
            diff  = lastest_weight - gbw
        else:
            gbw = '_'
            diff = '_'
        context['lastest_weight'] = lastest_weight
        context['ibw'] = ibw
        context['gbw'] = gbw
        context['diff'] = diff
        context['wibmr_one'] = wibmr_one
        context['wibmr_half'] = wibmr_half
        context['wlbmr_one'] = wlbmr_one
        context['wlbmr_half'] = wlbmr_half



        return context

@method_decorator(login_required, name='dispatch')
class GoalCreate(CreateView):
    model = Gbw
    form_class = GbwCreateForm
    success_url = reverse_lazy('goal')


    def get_initial(self):
        initial = super(GoalCreate, self).get_initial()
        initial.update({'user': self.request.user.id})
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()

        return super(GoalCreate, self).form_valid(form)

@method_decorator(staff_member_required, name='dispatch')
class DrugList(ListView):
    model = Drug
    context_object_name = 'drugs'
    paginate_by = 10

    


@method_decorator(login_required, name='dispatch')
class DrugView(TemplateView):
    template_name = 'sites/drug.html'
    

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        rxs = Rx.objects.filter(user = self.request.user)
        if rxs:
            drug = rxs.drugs
        else:
            drug = 'none'
        context['drug'] = drug
        return context

@method_decorator(login_required, name='dispatch')
class FoodProgramCreate(CreateView):
    model = FoodProgram
    form_class = FoodProgramForm
    success_url = reverse_lazy('food_list')


@method_decorator(login_required, name='dispatch')
class FoodCreate(CreateView):
    model = Food
    form_class = FoodCreateForm
    success_url = reverse_lazy('food_list')

@method_decorator(login_required, name='dispatch')
class FoodList(ListView):
    model = Food
    context_object_name = 'foods'
    paginate_by = 10


@method_decorator(login_required, name='dispatch')
class FoodShow(DetailView):
    model = Food

    def get_queryset(self):
        return self.model.objects


@method_decorator(login_required, name='dispatch')
class FoodUpdate(UpdateView):
    model = Food

    success_url = reverse_lazy('food_list')
    form_class = FoodCreateForm
    def get_queryset(self):
        return self.model.objects

    def get_initial(self):
        initial = super(FoodUpdate, self).get_initial()
        initial.update()
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.save()

        return super(FoodUpdate, self).form_valid(form)

@method_decorator(login_required, name='dispatch')
class FoodDelete(DeleteView):
    success_url = reverse_lazy('food_list')   
    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(FoodDelete, self).get_object()
        
        return obj

    

@method_decorator(login_required, name='dispatch')
class WeightCreate(CreateView):
    model = Weight
    form_class = WeightCreateForm
    success_url = reverse_lazy('weights_list')


    def get_initial(self):
        initial = super(WeightCreate, self).get_initial()
        initial.update({'user': self.request.user.id})
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()

        return super(WeightCreate, self).form_valid(form)




@method_decorator(login_required, name='dispatch')
class WeightList(ListView):
    model = Weight
    paginate_by = 10
    def toinch(self, height):
        return round(height /2.54,2) #last height in inch
    
    def age(self, born):
        return date.today().year-born.year
    
    def bmi(self, lastest_weight, lastest_height):
        return round(lastest_weight / (lastest_height/100 * lastest_height/100),2)
    
    def ibw(self, gender, lhi):
        if gender == 'M':
            return round(50 + (2.3 * (lhi-60)))
        else:
            return round(45.5 + (2.3 *(60 - lhi)))

    def bmr(self, lastest_weight, lastest_height, age, gender):
        if gender == 'M':
            return round(66 + (6.2 * lastest_weight) + (12.7 * lastest_height) - (6.76 * age))
        else:
            return round(655 + (4.35 * lastest_weight) + (4.7 * lastest_height) - (4.7 * age))
    
    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        weights = Weight.objects.filter(user = self.request.user).order_by('-date_created')
        weight = weights.values('weight',)
        if weight :
            height = weights.values('height',)
            person = User.objects.get(id= self.request.user.id)
            
            lastest_weight = weight.order_by('-date_created')[0]["weight"]
            lastest_height = height.order_by('-date_created')[0]["height"]
            bmi = self.bmi(lastest_weight, lastest_height)
            lhi = self.toinch(lastest_height)
            gender = person.gender
            born = person.birthday
            if born and gender:
                age = self.age(born)
                ibw = self.ibw(gender, lhi)
                bmr = self.bmr(lastest_weight, lastest_height, age, gender)
                wibmr_one = bmr + 1000
                wibmr_half = bmr +500
                wlbmr_one = bmr - 1000
                wlbmr_half = bmr -500
            else:
                message = '_'
                ibw = message
                bmr = message
                wibmr_one = message
                wibmr_half = message
                wlbmr_one = message
                wlbmr_half = message

        else:
            message = '_'
            bmi = message
            weights = message
            ibw = message
            bmr = message
            lastest_weight = message
            wibmr_one = message
            wibmr_half = message
            wlbmr_one = message
            wlbmr_half = message



        context['weights'] = weights
        context['bmi'] = bmi
        context['ibw'] = ibw
        context['bmr'] = bmr
        context['wibmr_one'] = wibmr_one
        context['wibmr_half'] = wibmr_half
        context['wlbmr_one'] = wlbmr_one
        context['wlbmr_half'] = wlbmr_half
        context['lastest_weight'] = lastest_weight

        return context



@method_decorator(login_required, name='dispatch')
class CreateDrug(CreateView):
    model = Drug
    form_class = DrugCreateForm
    success_url = reverse_lazy('Drug_create')


    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.save()

        return super(CreateDrug, self).form_valid(form)





@method_decorator(login_required, name='dispatch')
class WeightShow(DetailView):
    model = Weight

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


@method_decorator(login_required, name='dispatch')
class WeightUpdate(UpdateView):
    model = Weight

    success_url = reverse_lazy('weights_list')
    form_class = WeightCreateForm
    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def get_initial(self):
        initial = super(WeightUpdate, self).get_initial()
        initial.update({'user': self.request.user.id})
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()

        return super(WeightUpdate, self).form_valid(form)

@method_decorator(login_required, name='dispatch')
class WeightDelete(DeleteView):
    success_url = reverse_lazy('weights_list')   
    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(WeightDelete, self).get_object()
        if not obj.user == self.request.user:
            raise Http404
        return obj


@method_decorator(login_required, name='dispatch')
class ProfileUpdate(UpdateView):
    model = User
    
    success_url = reverse_lazy('profile')
    form_class = ProfileUpdateForm

    def get_queryset(self):
        return self.model.objects.filter(id=self.request.user.id)    


    def get_initial(self):
        initial = super(ProfileUpdate, self).get_initial()
        initial.update({'user': self.request.user.id})
        return initial

    def form_valid(self, form):
        """Force the user to request.user"""
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()

        return super(ProfileUpdate, self).form_valid(form)



@method_decorator(staff_member_required, name='dispatch')
class UserList(ListView):
    model = User
    paginate_by = 10


# @method_decorator(login_required, name='dispatch')
# class FoodList(ListView):
#     model = FoodDescription