from __future__ import unicode_literals
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from users.models import User
from rest_framework import serializers
from body.models import Weight, Drug, Rx


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs['email'], password=attrs['password'])

        if not user:
            raise serializers.ValidationError('Incorrect email or password.')

        if not user.is_active:
            raise serializers.ValidationError('User is disabled.')

        return {'user': user}



class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'last_login',
            'email',
            'is_active',
            'date_joined',
            'password'
        )
        read_only_fields = ('last_login', 'is_active', 'date_joined')
        extra_kwargs = {
            'password': {'required': True, 'write_only': True},

        }

    @staticmethod
    def validate_email(value):
        return validate_username(value)

    def create(self, validated_data):
        return User.objects.create_user(
                    validated_data.pop('email'),
                    validated_data.pop('password'),
                    **validated_data
)



class WeightUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", "email", "date_joined")



class WeightSerializer(serializers.ModelSerializer):
    user = WeightUserSerializer(read_only=True)
    class Meta:
        model = Weight
        fields = ("id", "user", "text", "weight", "height", "date_created")


class DrugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drug
        fields = ("id", "name", "description")

class RxSerializer(serializers.ModelSerializer):
    user = WeightUserSerializer(read_only=True)
    class Meta:
        model = Rx
        fields = ("id", "user", "drugs", "date_created")