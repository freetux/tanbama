import json
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from body.models import Weight
from body.serializers import WeightSerializer
from users.models import User
from rest_framework.authtoken.models import Token





class UserRegistrationAPIViewTestCase(APITestCase):
    url = reverse("users:list")

    def test_invalid_password(self):
        """
        Test to verify that a post call with invalid passwords
        """
        user_data = {
            "username": "testuser",
            "email": "test@testuser.com",
            "password": "password",
            "confirm_password": "INVALID_PASSWORD"
        }
        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_user_registration(self):
        """
        Test to verify that a post call with user valid data
        """
        user_data = {
            "username": "testuser",
            "email": "test@testuser.com",
            "password": "123123",
            "confirm_password": "123123"
        }
        response = self.client.post(self.url, user_data)
        self.assertEqual(201, response.status_code)
        self.assertTrue("token" in json.loads(response.content))

    def test_unique_username_validation(self):
        """
        Test to verify that a post call with already exists username
        """
        user_data_1 = {
            "username": "testuser",
            "email": "test@testuser.com",
            "password": "123123",
            "confirm_password": "123123"
        }
        response = self.client.post(self.url, user_data_1)
        self.assertEqual(201, response.status_code)

        user_data_2 = {
            "username": "testuser",
            "email": "test2@testuser.com",
            "password": "123123",
            "confirm_password": "123123"
        }
        response = self.client.post(self.url, user_data_2)
        self.assertEqual(400, response.status_code)


class UserLoginAPIViewTestCase(APITestCase):
    url = reverse("users:login")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)

    def test_authentication_without_password(self):
        response = self.client.post(self.url, {"username": "snowman"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(self.url, {"username": self.username, "password": "I_know"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(self.url, {"username": self.username, "password": self.password})
        self.assertEqual(200, response.status_code)
        self.assertTrue("token" in json.loads(response.content))


class UserLogoutAPIViewTestCase(APITestCase):
    url = reverse("users:logout")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_logout(self):
        response = self.client.post(self.url)
        self.assertEqual(200, response.status_code)
        self.assertFalse(Token.objects.filter(key=self.token).exists())




class WeightListCreateAPIViewTestCase(APITestCase):
    url = reverse("weights:list")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_weight(self):
        response = self.client.post(self.url, {"weight": 71,"text":"hello"})
        self.assertEqual(201, response.status_code)

    def test_user_weights(self):
        """
        Test to verify user weights list
        """
        Weight.objects.create(user=self.user, text="todays weight!", weight=72)
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content)) == Weight.objects.count())


class WeightDetailAPIViewTestCase(APITestCase):

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password)
        self.weight = Weight.objects.create(user=self.user, text="weight!", weight=70)
        self.url = reverse("weights:detail", kwargs={"pk": self.weight.pk})
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_weight_object_bundle(self):
        """
        Test to verify weight object bundle
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        weight_serializer_data = WeightSerializer(instance=self.weight).data
        response_data = json.loads(response.content)
        self.assertEqual(weight_serializer_data, response_data)

    def test_weight_object_update_authorization(self):
        """
            Test to verify that put call with different user token
        """
        new_user = User.objects.create_user("newuser", "new@user.com", "newpass")
        new_token = Token.objects.create(user=new_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)

        # HTTP PUT
        response = self.client.put(self.url, {"text": "edited!", "weight": 71})
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {"text": "edited!", "weight": 71})
        self.assertEqual(403, response.status_code)

    def test_weight_object_update(self):
        response = self.client.put(self.url, {"text": "edited!", "weight": 71})
        response_data = json.loads(response.content)
        weight = Weight.objects.get(id=self.weight.id)
        self.assertEqual(response_data.get("weight"), weight.weight)

    def test_weight_object_partial_update(self):
        response = self.client.patch(self.url, {"weight": 65})
        response_data = json.loads(response.content)
        weight = Weight.objects.get(id=self.weight.id)
        self.assertEqual(response_data.get("weight"), weight.weight)

    def test_weight_object_delete_authorization(self):
        """
            Test to verify that put call with different user token
        """
        new_user = User.objects.create_user("newuser", "new@user.com", "newpass")
        new_token = Token.objects.create(user=new_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_weight_object_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)
