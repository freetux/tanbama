from django.conf.urls import url, include
from .views import UserRegistrationAPIView, WeightListCreateAPIView, WeightDetailAPIView, DrugListCreateAPIView, RxListCreateAPIView, RxDetailAPIView, DrugDetailAPIView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

api_user = [
    url(r'^$', UserRegistrationAPIView.as_view(), name="list"),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    
]

api_body = [
    url(r'^weight/$', WeightListCreateAPIView.as_view(), name="list"),
    url(r'^weight/(?P<pk>[0-9]+)/$', WeightDetailAPIView.as_view(), name="detail"),
    url(r'^drug/$', DrugListCreateAPIView.as_view(), name="list"),
    url(r'^drug/(?P<pk>[0-9]+)/$', DrugDetailAPIView.as_view(), name="detail"),
    url(r'^rx/$', RxListCreateAPIView.as_view(), name="list"),
    url(r'^rx/(?P<pk>[0-9]+)/$', RxDetailAPIView.as_view(), name="detail"),
    
]

    


urlpatterns = [
    url(r'^body/', include(api_body)),
    url(r'^user/', include(api_user)),
]