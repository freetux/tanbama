from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView, CreateAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status, permissions
from body.models import Weight, Drug, Rx
from .permissions import UserIsOwnerWeight, IsOwnerOrAdmin
from rest_framework.permissions import IsAdminUser
from .serializers import WeightSerializer, DrugSerializer, RxSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer, UserLoginSerializer
import jwt
from rest_framework_jwt.utils import jwt_payload_handler
from tanbama import settings


class UserRegistrationAPIView(CreateAPIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        user = serializer.instance
        payload = jwt_payload_handler(user=user)
        token = jwt.encode(payload, settings.SECRET_KEY)
        token = token.decode('unicode_escape')
        data = serializer.data
        data["token"] = token
        headers = self.get_success_headers(serializer.data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)


class WeightListCreateAPIView(ListCreateAPIView):
    serializer_class = WeightSerializer

    def get_queryset(self):
        return Weight.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class WeightDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = WeightSerializer
    queryset = Weight.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwnerWeight)


    
class DrugListCreateAPIView(ListCreateAPIView):
    serializer_class = DrugSerializer
    permission_classes = (IsAdminUser,)    

    def get_queryset(self):
        return Drug.objects.all()

    def perform_create(self, serializer):
        serializer.save()


class DrugDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = DrugSerializer
    queryset = Drug.objects.all()
    permission_classes = (IsAdminUser)   


class RxListCreateAPIView(ListCreateAPIView):
    serializer_class = RxSerializer
    permission_classes = (IsAdminUser,)

    def get_queryset(self):
        return Rx.objects.all()

    def perform_create(self, serializer):
        serializer.save()


class RxDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RxSerializer
    queryset = Rx.objects.all()
    permission_classes = (IsOwnerOrAdmin)   


class UserRxtDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RxSerializer
    queryset = Rx.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwnerWeight)   
    
    # def bmi(self, height, weight):
    #     data = Weight.objects.latest('date_added')
    #     weight = data.weight
    #     height = data.height/100
    #     return weight/height**2


