from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

api_urls = [
    url(r'^', include('api.urls')),    
    
]
urlpatterns = [
    url(r"^messages/", include("pinax.messages.urls", namespace="pinax_messages")),
    url('', include('django.contrib.auth.urls')),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^',include('sites.urls')),
    url(r'^site/',include(api_urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urls)),
    url(r'^accounts/', include('users.urls')),
    url(r'^select2/', include('django_select2.urls')),
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)