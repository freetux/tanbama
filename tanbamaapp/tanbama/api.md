# API Usage Guide:

### API Endpoints

#### Users

* **/api/users/** (User registration endpoint)

allow methods:post
example:
POST:   

curl -X POST -H 'Content-Type: application/json' -d "username=yourusername&password=yourpassword&email=your@demoemail.com" yourdomain/api/users/


output:

give you an token


* **/api/users/login/** (User login endpoint)

curl -X POST -H 'Content-Type: application/json' -d "username=yourusername&password=yourpassword" yourdomain/api/users/login

output:

give you an token

* **/api/users/logout/** (User logout endpoint)

curl -X POST -H 'Content-Type: application/json' -d "username=yourusername&password=yourpassword" yourdomain/api/users/logout

output:

delete your token from database.dont worry you can login for another one!

#### Weights

* **/api/weights/** (Weight create and list endpoint)

Allowed methods:GET, POST

GET:

curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET http://localhost:8000/api/weights


output:

list of your height and weights

POST

curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"weight":70,"height":182,"text":"demo"}' -X POST http://localhost:8000/api/weights


output:

create an weight and height to database



* **/api/weights/{weight-id}/** (Weight retrieve, update and destroy endpoint)

