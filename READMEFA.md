## تنباما

سامانه مراقبت جمعی سلامت

زبان های دیگر: [english](https://gitlab.com/biomanw/tanbama/blob/master/README.md)

### API Endpoints


#### user


* **/api/user/** (User registration endpoint)

allow methods:post

مثال:


POST:   
```
curl -X POST  -d "email=youremail&password=yourpassword" tanbama.ir/api/user/
```
خروجی:

  یک توکن منحصر بفرد به همراه اطلاعات کاربر .


* **/api/user/api-token-auth/**

یک توکن JWT به همراه اطلاعات کاربر فراهم میکند


allow methods:post

مثال:


POST:
```
curl -X POST -d "username=yourusername&password=yourpassword" tanbama.ir/api/api-token-auth/
```
خروجی:


  token: "yourtoken" and "user data"

* **/api/user/api-token-refresh/**

  با این api شما میتوانید توکن خود را تازه کنید 

allow methods:post

مثال:


POST:
```
curl -X POST -H "Content-Type: application/json" -d '{"token":"<EXISTING_TOKEN>"}' tanbama.ir/api/api-token-refresh/
```


#### body/weights

* **/api/body/weights/** (مشاهده لیست و ایجاد قد و وزن)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/weights
```

خروجی:

  لیست قد و وزن شما

POST
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"weight":70,"height":182,"text":"demo"}' -X POST tanbama.ir/api/body/weights
```

خروجی:

  ایجاد یک رکورد قد و وزن در دیتابیس

* **/api/body/weights/{weight-id}/** (ویرایش و حذف قد و وزن)


#### body/drug


* **/api/body/drug/** (مشاهده لیست و ایجاد دارو)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/drug
```

خروجی:

  لیست داروها

POST:

```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"name":drug-name,"description":"drug-description"}' -X POST tanbama.ir/api/body/drug
```

خروجی:

  افزودن دارو به دیتابیس فقط کاربر مدیر اجازه دارد!

* **/api/body/drug/{drug-id}/** (بروزرسانی و حذف دارو)

 فقط کاربر مدیر اجازه دارد



#### body/rx


* **/api/body/rx/** (مشاهده لیست و ایجاد نسخه)

Allowed methods:GET, POST

GET:
```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json"  -X GET tanbama.ir/api/body/rx
```

خروجی:

  لیست نسخه ها
POST:

```
curl -H "Authorization: JWT yourtoken" -H "Content-type: application/json" -H "Accept: application/json" -d '{"user":user,"drugs":"drugslist"}' -X POST tanbam  a.ir/api/body/rx
```

output:

  افزودن یک رکورد نسخه به دیتابیس کاربر مدیر اجازه دارد

* **/api/body/rx/{rx-id}/** (ویرایش و حذف نسخه)

  کاربر مدیر اجازه دارد

### نصب

```
pip install -r requirements.txt
```
### استفاده

#### مشارکت در توسعه:
 
گام نخست:
از این repository یک clone بگیرید.

با این دستور:
```
cd
git clone https://gitlab.com/biomanw/tanbama.git
```


#### به branch فرعی پروژه یعنی dev سویج کنید

```
git checkout dev
```

#### تغییرات خود را اعمال کنید و بعد :



```
git add .
#با این دستور تغییرات را به مخزن محلی خود اضافه کنید.
```

```
git commit -m "your comment for this commit"
#با این دستور تغییرات ردیابی شده را برای هدایت به مخزن پروژه آماده میکنید.
```

```
git push origin dev
#و در نهایت با این دستور تغییرات مخزن محلی خود را به مخزن شاخه توسعه پروژه ارسال میکنید
```

نیازمندی‌ها:


1.python 3

2.pip

### راه اندازی

۱.با دستور زیر یک محیط مجازی پایتون ۳ راه بیندازید
```
virtualenv -p python3 tanbamaenv
```

۲.با دستور زیر به محیط مجازی بروید


```
source tanbamaenv/bin/activate
```


۳.با این دستور نیازمندی های پروژه را نصب کنید


```
pip install -r requirements.txt
```

۴.با این دستور اولین جداول دیتابیس را بسازید

```
cd tanbamaapp
python manage.py migrate
```


۵- با این دستور دایرکتوری فایل های ایستای پروژه را ایجاد و بروز کنید


```
python manage.py collectstatic
```


اگر همه چیز خوب پیش برود اماده بالا امدن پروژه روی فضای محلی خود باشید!
و البته مرورگر خود را باز کنید و به آدرس زیر بروید

localhost:8000/


```
cd tanbamaapp
python manage.py runserver
```



درصورت برخوردن به هر مشکلی میتوانید از بخش [issue](https://gitlab.com/dashboard/issues?assignee_id=1968146) مشکل خود را مطرح کنید تا رسیدگی شود


